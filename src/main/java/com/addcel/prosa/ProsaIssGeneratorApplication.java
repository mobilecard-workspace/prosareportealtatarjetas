package com.addcel.prosa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProsaIssGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProsaIssGeneratorApplication.class, args);
	}

}
