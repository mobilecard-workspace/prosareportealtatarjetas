package com.addcel.prosa.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssNewDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String user;
	private String password;
	private String recordId;
	private String dataGroup;
	private String messageCode;
	private String issuer;
	private Integer branch;
	private Integer product;
	private String clientBankId;
	private String documentType;
	private String documentNumber;
	private String card;
	private String primaryCard;
	private String affinityGroup;
	private String date;
	private String processingDate;
	private String proposalReference;
	private String cardType;
	private String costCode;
	private String expirationDate;
	private String validityMonths;
	private char vip;
	private String embossedName;
	private String pin;
	private String track1;
	private String track2;
	private String telephonePassword;
	private String telephonePasswordVdate;
	private String emailData;
	private char sendTypeData;
	private String deliverCompany;
	private String embossingCompany;
	private String birthDate;
	private String birthPlace;
	private char gender;
	private String martialStatus;
	private String fullName;
	private String occupation;
	private String lineOfBussiness;
	private String quantityOfEmployes;
	private String rg;
	private String organization;
	private String stateOfIssuance;
	private char overLimitUser;
	private String lineGroup;
	private String statementCurrency;
	private String bankAccount;
	private String passwordCue;
	private String language;
	private String level;
	private String salesChannel;
	private String saleCampaign;
	private String salePromoter;
	private char consignationCheck;
	private char summaryRemittance;
	private String father;
	private String mother;
	private String plasticColor;
	private String cardholderName;
	private String cardHolderLastName;
	private String cardHolderTitle;
	private char applyDefaultServices;
	private char photoIndicator;
	private char relationshipLevel;
	private String pamcardNumber;
	private String cardCarrierNumber;
	private char cardHolderType;
	private char driverType;
	private String via;
	private char loanAllowed;
	private String merchantCode;
	private Double accountBonification;
	private char specialNeeds;
	private Double mpBon;
	private String filler;
}
